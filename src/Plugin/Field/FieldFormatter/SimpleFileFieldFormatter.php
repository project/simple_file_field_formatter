<?php

namespace Drupal\simple_file_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Simple file field' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_file_field_formatter",
 *   label = @Translation("Simple File field formatter"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class SimpleFileFieldFormatter extends FormatterBase {

  /**
   * The entity type manager service
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Construct a FileFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $file_field = $this->entityFieldManager->getFieldDefinitions('file', 'file');
    $options = [];
    foreach ($file_field as $field_name => $field) {
      $options[$field_name] = $field->getLabel();
    }
    $options['description'] = $this->t('Description');
    $form['fields'] = [
      '#title' => $this->t('Fields to display'),
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $options,
      '#default_value' => $this->getSetting('fields'),
    ];
    $form['delimiter'] = [
      '#title' => $this->t('Delimiter'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('delimiter')?? ' ',
    ];

    return $form;
  }

  public static function defaultSettings() {
    return [
        'fields' => ['filename'],
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $fields = $this->getSetting('fields');
    $summary[] = $this->t('Displays fields :fields of file', [':fields' => $fields]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $field_display = $this->getSetting('fields');
    $delimiter = $this->getSetting('delimiter')??' ';
    /**
     * @var  $delta
     * @var  $item \Drupal\file\Plugin\Field\FieldType\FileItem
     */
    $storage = $this->entityTypeManager->getStorage('file');
    foreach ($items as $delta => $item) {
      $item_value = $item->getValue();
      if (!$item instanceof FileItem || empty($item_value['target_id'])) {
        continue;
      }
      // Load file.
      $file = $storage->load($item_value['target_id']);
      if (!$file instanceof File) {
        continue;
      }
      $out = [];
      foreach ($field_display as $field) {
        if (empty($field)) {
          continue;
        }
        // Special field for description.
        if (!empty($item->{$field}) && !$file->hasField($field)) {
          $out[] = $item->{$field};
          continue;
        }
        // Get the value of field inside the file entity.
        $file_field_value = $file->get($field);
        $file_value = [];
        foreach ($file_field_value->getIterator() as $file_field_item) {
          $main_property = $file_field_item->mainPropertyName();
          $field_value = $file_field_item->getValue();
          if (!empty($field_value[$main_property])) {
            $file_value[] = $field_value[$main_property];
          }
        }
        $value = implode($delimiter, $file_value);
        if ($field == 'filesize') {
          $value = format_size($value);
        }
        $out [] = $value;
      }
      // Render each element as markup.
      $element[$delta] = ['#markup' => implode($delimiter, $out)];
    }

    return $element;
  }

}
